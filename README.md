# Research instrumention for PSM, JIPMER 

## LICENSE 

* Android - GPL v3
* Backend - 
* Web - 
* Art - CC BY SA 4.0

## Repository Structure 

* Monorepo format
* All meta information is in root
* Each code system will have its own master folder 

## Meta information

* STORIES.md - Contains user stories 
* ARCHITECTURE.md - Contains data models, REST API, android functionalities
* PROPOSAL.md - Timeline, cost estimations etc

## DEPLOYMENT
