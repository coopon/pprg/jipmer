package pondy.coopon.org.pprgjipmer;

import android.app.Application;

import java.io.File;
import java.util.Objects;

import pondy.coopon.org.pprgjipmer.utils.CustomExceptionHandler;

public class PprjApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if(!(Thread.getDefaultUncaughtExceptionHandler() instanceof CustomExceptionHandler)) {
            File internalStorage = new File(Objects.requireNonNull(getApplicationContext()).getFilesDir().getAbsolutePath());
            Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(this, internalStorage.toString(), new String[] {"hello@cooponscitech.in", "manimaraninam1027@gmail.com"}));
        }
    }

}
