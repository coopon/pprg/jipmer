package pondy.coopon.org.pprgjipmer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.helper.SharedPref;
import pondy.coopon.org.pprgjipmer.model.User;
import pondy.coopon.org.pprgjipmer.rest.ApiClient;
import pondy.coopon.org.pprgjipmer.rest.PPRGInterface;
import pondy.coopon.org.pprgjipmer.utils.GeneralUtils;
import pondy.coopon.org.pprgjipmer.utils.ToastUtils;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    // Helper
    private SharedPref sharedPref;

    // Views
    @BindView(R.id.edit_user_name)
    EditText editUserName;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.btn_mother)
    Button btnMother;
    @BindView(R.id.smooth_progress_bar)
    SmoothProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Butter knife Init
        ButterKnife.bind(this);

        sharedPref = new SharedPref(getApplicationContext());

        // Check user already login or Not
        if (sharedPref.isLoggedIn()) {
            openMainScreen();
        }

        btnLogin.setOnClickListener(v -> {
            if (GeneralUtils.isNetworkConnected(getApplicationContext())) {
                if (isValidFields())
                    callLoginApi();
            } else
                ToastUtils.showWarning(getApplicationContext(), getString(R.string.msg_check_network_connection));
        });

        btnMother.setOnClickListener(v ->{
            if (GeneralUtils.isNetworkConnected(getApplicationContext())) {
                sharedPref.setMotherLogin();
                openMainScreen();
            } else
                ToastUtils.showWarning(getApplicationContext(), getString(R.string.msg_check_network_connection));
        });
    }

    /**
     * Login Api
     */
    private void callLoginApi() {

        progressBar.setVisibility(View.VISIBLE);

        Retrofit retrofit = ApiClient.getRetrofit();
        PPRGInterface pprgInterface = retrofit.create(PPRGInterface.class);

        JsonObject credentialsParams = new JsonObject();
        credentialsParams.addProperty("email", editUserName.getText().toString());
        credentialsParams.addProperty("password", editPassword.getText().toString());
        pprgInterface.authenticate(credentialsParams).enqueue(new retrofit2.Callback<User>() {
            @Override
            public void onResponse(@NonNull retrofit2.Call<User> call, @NonNull retrofit2.Response<User> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getId() != null) {
                    User user = response.body();
                    sharedPref.createLoginSession(user);
                    ToastUtils.showSuccess(getApplicationContext(), getString(R.string.msg_successful_login));
                    openMainScreen();
                } else {
                    ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_credential));
                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<User> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.INVISIBLE);
                ToastUtils.showError(getApplicationContext(), getString(R.string.msg_check_network_connection));
            }
        });
    }

    /**
     * Open the Main screen Activity
     */
    private void openMainScreen() {
        Intent outgoingIntent = new Intent(getApplicationContext(), sharedPref.isMotherLoggedIn() ? MothersMainActivity.class :  MainActivity.class);
        startActivity(outgoingIntent);
        finish();
    }

    /**
     * Validation of every input fields
     * Check Whether input is correct or wrong
     *
     * @return boolean
     * true    - valid fields
     * false   - Invalid fields
     */
    private boolean isValidFields() {

        if (!TextUtils.isEmpty(editUserName.getText())) {
            if (!TextUtils.isEmpty(editPassword.getText()))
                return true;
            else
                ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_password));
        } else
            ToastUtils.showError(getApplicationContext(), getString(R.string.error_invalid_user_name));

        return false;
    }
}
