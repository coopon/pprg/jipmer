package pondy.coopon.org.pprgjipmer.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.fragments.ContentsViewFragment;
import pondy.coopon.org.pprgjipmer.helper.SharedPref;
import pondy.coopon.org.pprgjipmer.utils.GeneralUtils;

public class MothersMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(android.R.id.content, new ContentsViewFragment(), "myFragmentTag");
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options_mother, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout:
                logout();
                return true;
            case R.id.menu_about_us:
                GeneralUtils.aboutUsActivity(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        new AlertDialog.Builder(MothersMainActivity.this)
                .setTitle(getString(R.string.hint_confirmation))
                .setMessage(getString(R.string.msg_do_you_want_logout))
                .setPositiveButton(android.R.string.yes,
                        (dialog, whichButton) -> {
                            SharedPref sharedPref = new SharedPref(getApplicationContext());
                            sharedPref.logoutUser(); // Clear session details*/
                        })
                .setNegativeButton(android.R.string.no,
                        null)
                .show();
    }
}