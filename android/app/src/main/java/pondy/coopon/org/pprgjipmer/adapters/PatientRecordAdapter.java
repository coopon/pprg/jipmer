package pondy.coopon.org.pprgjipmer.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.couchbase.lite.CouchbaseLiteException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.activity.AddEditBioDataActivity;
import pondy.coopon.org.pprgjipmer.activity.MainActivity;
import pondy.coopon.org.pprgjipmer.activity.PatientTabActivity;
import pondy.coopon.org.pprgjipmer.helper.DatabaseManager;
import pondy.coopon.org.pprgjipmer.interfaces.MapInterface;
import pondy.coopon.org.pprgjipmer.interfaces.PatientCardClickListener;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.utils.DateTimeUtils;
import pondy.coopon.org.pprgjipmer.utils.GeneralUtils;
import pondy.coopon.org.pprgjipmer.utils.ToastUtils;
import pondy.coopon.org.pprgjipmer.utils.constant.PatientRecordConst;

public class PatientRecordAdapter extends RecyclerView.Adapter<PatientRecordAdapter.ViewHolder> {

    private List<Mother> mPatientList = new ArrayList<>();
    private Context mContext;

    private PatientCardClickListener mListener;
    private Integer cardType;

    public PatientRecordAdapter(Context context, Integer cardType) {
        this.mContext = context;
        this.cardType = cardType;
    }

    public PatientRecordAdapter(Context mContext, Integer cardType, PatientCardClickListener mListener) {
        this.mContext = mContext;
        this.mListener = mListener;
        this.cardType = cardType;
    }

    public void setPatients(List<Mother> mothers) {
        this.mPatientList = mothers;
    }

    private Integer getCardType() {
        return cardType;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.patient_record_card_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        Mother patient = mPatientList.get(holder.getAdapterPosition());

        holder.txtName.setText(patient.getName() != null ? patient.getName() : "");
        String about = "<b>Age : </b>" + (patient.getAge() != null ? patient.getAge() : "xx") +
                "\t\t\t" + "<b>Blood : <font color=\"" + ContextCompat.getColor(mContext, R.color.blood_color) + "\">" + ((patient.getBloodGroup() != null ? patient.getBloodGroup() : "xx") + "</font></b>");
        holder.txtAbout.setText(Html.fromHtml(about));
        String address =
                (patient.getDrNo() != null && patient.getDrNo().length() > 0 ? patient.getDrNo() + ", " : "")
                        + (patient.getStreetName() != null && patient.getStreetName().length() > 0 ? patient.getStreetName() + ", " : "")
                        + (patient.getArea() != null && patient.getArea().length() > 0 ? patient.getArea() + ", " : "")
                        + (patient.getCity() != null && patient.getCity().length() > 0 ? patient.getCity() + ", " : "")
                        + (patient.getPinCode() != null && patient.getPinCode().length() > 0 ? "Pin-" + patient.getPinCode() + ", " : "");
        holder.txtArea.setText(address);

        holder.txtActualVisit.setText(String.valueOf(patient.getCurrentVisit() != null ? (patient.getCurrentVisit() > 0 ? patient.getCurrentVisit() - 1 : 0) : 0));
        holder.txtNoVisits.setText("/");
        holder.txtNoVisits.append(String.valueOf(patient.getVisits() != null ? patient.getVisits().size() : 0));

        boolean isThresholdExist = true;

        /*
            Check current visit schedule date before the 2 days
            If yes -> Highlighted
          */
        if (patient.getVisits() != null) {
            if (patient.getVisits().size() > 0 && patient.getVisits().size() >= patient.getCurrentVisit()) {
                Calendar currentVisit = DateTimeUtils.convertStringDateTimeToCalender(patient.getVisits().get(patient.getCurrentVisit() - 1).getSchedDate(), DateTimeUtils.dateFormat_api);
                Calendar dayBeforeYesterday = Calendar.getInstance();
                dayBeforeYesterday.add(Calendar.DAY_OF_YEAR, -2); // 2 days before
                if (currentVisit.after(dayBeforeYesterday))
                    isThresholdExist = false;
            } else
                isThresholdExist = false;
        } else
            isThresholdExist = false;

        if(!isThresholdExist && patient.getCurrentVisit() > 6)
            holder.layoutVisit.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_visit_info_completed));
        else
            holder.layoutVisit.setBackground(ContextCompat.getDrawable(mContext, isThresholdExist ? R.drawable.bg_visit_info_indicator : R.drawable.bg_visit_info_normal));
        holder.txtActualVisit.setTextColor(isThresholdExist ? Color.WHITE : Color.BLACK);
        holder.txtNoVisits.setTextColor(isThresholdExist ? Color.WHITE : Color.BLACK);

        holder.layoutRecord.setOnClickListener(view -> {
            if (mListener != null) {
                switch (getCardType()) {
                    case PatientRecordConst.SCHEDULE:
                        mListener.onClick(patient);
                        break;
                    case PatientRecordConst.BASE:
                        openPatientTabActivity(patient.getMainId());
                        break;
                }
            } else {
                openPatientTabActivity(patient.getMainId());
            }
        });

        holder.btnMap.setOnClickListener(view -> {
            if (patient.getLocation() != null && patient.getLocation().contains(",")) {
                /*
                 * call MapInterface's method in DashboardActivity context
                 * in order to pass the location data to MapViewFragment
                 */
                MapInterface mapInterface = (MainActivity) mContext;
                mapInterface.patientLocation(GeneralUtils.convertStringToLocation(patient.getLocation()), patient.getMainId(), patient.getName());
            } else
                ToastUtils.showWarning(mContext, patient.getName() + ", " + patient.getArea() + " - " + mContext.getString(R.string.msg_no_location));
        });

        holder.btnCall.setOnClickListener(v -> {
            if (patient.getPhone() != null) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + patient.getPhone()));
                mContext.startActivity(callIntent);
            } else
                ToastUtils.showWarning(mContext, mContext.getString(R.string.msg_phone_no_not_found));
        });

        holder.btnEdit.setOnClickListener(v -> openPatientEditActivity(patient.getMainId()));

        holder.btnDelete.setOnClickListener(v -> {

            new AlertDialog.Builder(mContext)
                    .setTitle("Confirmation")
                    .setMessage("Do you want to Delete " + patient.getName() + " record ?")
                    .setPositiveButton(android.R.string.yes, (dialog, whichButton) ->
                    {
                        DatabaseManager dbMgr = DatabaseManager.getSharedInstance(mContext);
                        // Query documents that have been deleted
                        try {
                            dbMgr.database.delete(dbMgr.database.getDocument(patient.getMainId()));
                        } catch (CouchbaseLiteException e) {
                            e.printStackTrace();
                        }
                    })
                    .setNegativeButton(android.R.string.no,
                            null)
                    .show();

        });

        holder.btnMap.setVisibility(getCardType() == PatientRecordConst.MAP ? View.GONE : View.VISIBLE);
    }

    private void openPatientTabActivity(String patientId) {
        Intent outgoing = new Intent(mContext, PatientTabActivity.class);
        outgoing.putExtra("PATIENT_ID", patientId);
        mContext.startActivity(outgoing);
    }

    private void openPatientEditActivity(String patientId) {
        Intent outgoing = new Intent(mContext, AddEditBioDataActivity.class);
        outgoing.putExtra("PATIENT_ID", patientId);
        mContext.startActivity(outgoing);
    }

    @Override
    public int getItemCount() {
        return mPatientList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_patient_avatar)
        ImageView imgAvatar;
        @BindView(R.id.txt_patient_name)
        TextView txtName;
        @BindView(R.id.txt_patient_about)
        TextView txtAbout;
        @BindView(R.id.txt_patient_area)
        TextView txtArea;
        @BindView(R.id.btn_map)
        ImageButton btnMap;
        @BindView(R.id.btn_call)
        ImageButton btnCall;
        @BindView(R.id.btn_edit)
        ImageButton btnEdit;
        @BindView(R.id.btn_delete)
        ImageButton btnDelete;
        @BindView(R.id.txt_actual_visit)
        TextView txtActualVisit;
        @BindView(R.id.txt_no_of_visits)
        TextView txtNoVisits;
        @BindView(R.id.layout_visit_info)
        RelativeLayout layoutVisit;
        @BindView(R.id.card_patient_record)
        CardView layoutRecord;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            btnEdit.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
        }
    }
}
