package pondy.coopon.org.pprgjipmer.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.interfaces.PatientTaskUpdateListener;
import pondy.coopon.org.pprgjipmer.interfaces.SubjectUpdateListener;
import pondy.coopon.org.pprgjipmer.interfaces.TaskUpdateListener;
import pondy.coopon.org.pprgjipmer.model.CheckList;
import pondy.coopon.org.pprgjipmer.model.Subject;
import pondy.coopon.org.pprgjipmer.model.Task;
import pondy.coopon.org.pprgjipmer.model.Visit;
import pondy.coopon.org.pprgjipmer.model.VisitType;
import pondy.coopon.org.pprgjipmer.utils.AppConfig;

public class PatientTaskTypeAdapter extends RecyclerView.Adapter<PatientTaskTypeAdapter.ViewHolder> {

    private List<VisitType> modelList;
    private Subject subject =  new Subject();
    private Context mContext;
    private PatientTasksAdapter adapter = null;
    private SubjectUpdateListener mListener;

    public PatientTaskTypeAdapter(Context context, List<VisitType> lineModels, SubjectUpdateListener listener) {
        this.mContext = context;
        this.modelList = lineModels;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_type, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int p) {

        int pos = holder.getAdapterPosition();

        VisitType master = modelList.get(pos);


        TaskUpdateListener taskUpdateListener = (tag, taskList) -> {
            updateSubjectValue(tag, taskList);
            mListener.onSubjectUpdate(getSubjectInfo());
        };

        adapter = new PatientTasksAdapter(master.getTaskList(), taskUpdateListener, modelList.get(pos).getTag());


        holder.recyclerView.setAdapter(adapter);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        holder.txtType.setText(master.getType() != null ? master.getType() : "Task");

        // Init subject
        updateSubjectValue(master.getTag(), master.getTaskList());
    }

    private void updateSubjectValue(String tag, List<Task> taskList) {
        switch (tag){
            case AppConfig.QuestionTypeAsk :
                subject.setAsk(taskList);
                break;
            case AppConfig.QuestionTypeExamine :
                subject.setExamine(taskList);
                break;
            case AppConfig.QuestionTypeCounsel :
                subject.setCounsel(taskList);
                break;
        }
    }


    @Override
    public int getItemCount() {
        return modelList.size();
    }

    /* adapter view holder */
    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_type)
        TextView txtType;
        @BindView(R.id.recycler_view__question)
        RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public Subject getSubjectInfo(){
        return subject;
    }
}