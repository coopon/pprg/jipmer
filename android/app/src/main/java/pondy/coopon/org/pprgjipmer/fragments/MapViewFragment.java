package pondy.coopon.org.pprgjipmer.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.ListenerToken;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryChange;
import com.couchbase.lite.QueryChangeListener;
import com.couchbase.lite.Result;
import com.couchbase.lite.ResultSet;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import org.oscim.android.MapView;
import org.oscim.core.BoundingBox;
import org.oscim.event.Gesture;
import org.oscim.event.GestureListener;
import org.oscim.event.MotionEvent;
import org.oscim.layers.GroupLayer;
import org.oscim.layers.Layer;
import org.oscim.layers.LocationLayer;
import org.oscim.layers.marker.ItemizedLayer;
import org.oscim.layers.marker.MarkerItem;
import org.oscim.layers.marker.MarkerSymbol;
import org.oscim.layers.tile.buildings.BuildingLayer;
import org.oscim.layers.tile.vector.VectorTileLayer;
import org.oscim.layers.tile.vector.labeling.LabelLayer;
import org.oscim.map.Map;
import org.oscim.theme.VtmThemes;
import org.oscim.tiling.source.mapfile.MapFileTileSource;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.activity.PatientTabActivity;
import pondy.coopon.org.pprgjipmer.adapters.PatientRecordAdapter;
import pondy.coopon.org.pprgjipmer.helper.DatabaseManager;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.utils.FileUtils;
import pondy.coopon.org.pprgjipmer.utils.GPSTracker;
import pondy.coopon.org.pprgjipmer.utils.GeneralUtils;
import pondy.coopon.org.pprgjipmer.utils.MapUtils;
import pondy.coopon.org.pprgjipmer.utils.constant.PatientRecordConst;

public class MapViewFragment extends Fragment {

    private static final String TAG = "MapViewFragment";

    // Helper
    private DatabaseManager dbMgr;
    private GPSTracker gpsTracker;

    // variables for external storage and map files
    private static String areaName = "pondicherry";


    @BindView(R.id.fab_menu)
    FloatingActionMenu btnFabMenu;
    @BindView(R.id.fab_locate_me)
    FloatingActionButton btnLocateMe;
    @BindView(R.id.fab_clear_markers)
    FloatingActionButton btnClearMarkers;
    @BindView(R.id.fab_show_all_markers)
    FloatingActionButton btnShowAllMarkers;
    @BindView(R.id.recycler_view_patient)
    RecyclerView recyclerView;
    @BindView(R.id.patient_bottom_sheet)
    LinearLayout layoutBottomSheet;
    @BindView(R.id.mapView)
    MapView mapView;

    private BottomSheetBehavior bottomSheetBehavior;
    private ItemizedLayer<MarkerItem> patientMarkerLayer;
    private BoundingBox bbox;
    private LocationLayer locationLayer;
    private AlertDialog alert;

    private Query query;
    private ListenerToken queryListenerToken;

    public MapViewFragment() {
        // Required empty public constructor
        Log.d(TAG, "MapViewFragment: called");
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: called");

        dbMgr = DatabaseManager.getSharedInstance(getContext());
        gpsTracker = new GPSTracker(Objects.requireNonNull(getActivity()));
        prepareMap();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.d(TAG, "onCreateView: called");

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map_view, container, false);

        ButterKnife.bind(this, view);

        bottomSheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);


        // Find current location
        btnLocateMe.setOnClickListener(v -> {
            locationLayer.setEnabled(true);     // Location layer enable
            gpsTracker.initLocationManager();   // Init location manager
            gpsTracker.startLocationListener(locationListener); // Start Location listener
        });

        // Clear markers and location
        btnClearMarkers.setOnClickListener(v -> {
            clearMarkers();
            locationLayer.setEnabled(false);        // Location layer disable
            gpsTracker.destroyLocationManager();    // Remove Location manager & stop location listener
        });

        // Show all user location markers
        btnShowAllMarkers.setOnClickListener(v -> loadAllPatientsToMap());

        return view;
    }

    // Location listener
    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            // Update in location layer & map view
            locationLayer.setPosition(location.getLatitude(), location.getLongitude(), location.getAccuracy());
            mapView.map().updateMap(true);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadMap(); // Load map all init views, layers, bounding box
    }

    /**
     * Prepare map files from graphghoper zip file
     */
    private void prepareMap() {
        File internalStorage = new File(Objects.requireNonNull(getContext()).getFilesDir().getAbsolutePath());
        File mapZip = new File(internalStorage.toString() + "/" + areaName + "-gh");

        if (!mapZip.exists()) {
            /* copy from asset folder */
            try {
                String[] assetFiles = getResources().getAssets().list("");
                assert assetFiles != null;
                for (String fileName : assetFiles) {
                    if (fileName.equals(areaName + "-gh.zip")) {
                        FileUtils.unZip(getResources().getAssets().open(fileName), internalStorage);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Load map init views, layers, bounding box
     */
    private void loadMap() {
        File areaFolder = new File(Objects.requireNonNull(getContext()).getFilesDir(), areaName + "-gh");
        try {
            // add touch listeners to map
            mapView.map().layers().add(new MapEventsReceiver(mapView.map()));

            MapFileTileSource mapFileTileSource = new MapFileTileSource();
            mapFileTileSource.setMapFile(new File(areaFolder, areaName + ".map").getAbsolutePath());

            VectorTileLayer vectorTileLayer = mapView.map().setBaseMap(mapFileTileSource);
            bbox = mapFileTileSource.getMapInfo().boundingBox;

            // add building and labels to the map
            GroupLayer groupLayer = new GroupLayer(mapView.map());
            groupLayer.layers.add(new BuildingLayer(mapView.map(), vectorTileLayer));
            groupLayer.layers.add(new LabelLayer(mapView.map(), vectorTileLayer));
            mapView.map().layers().add(groupLayer);

            // add location layer
            locationLayer = new LocationLayer(mapView.map());
            locationLayer.locationRenderer.setShader("location_1_reverse");
            mapView.map().layers().add(locationLayer);

            // add patient marker layer
            patientMarkerLayer = new ItemizedLayer<>(mapView.map(), (MarkerSymbol) null);
            patientMarkerLayer.setOnItemGestureListener(new ItemizedLayer.OnItemGestureListener<MarkerItem>() {
                @Override
                public boolean onItemSingleTapUp(int index, MarkerItem item) {
                    if (item.getTitle() != null) {
                        // clear any previous listener if any
                        layoutBottomSheet.setOnClickListener(null);

                        final String patient_id = item.getSnippet();
                        Mother patient = dbMgr.getPatient(patient_id);

                        PatientRecordAdapter adapter = new PatientRecordAdapter(getContext(), PatientRecordConst.MAP);
                        List<Mother> list = new ArrayList<>();
                        list.add(patient);
                        adapter.setPatients(list);
                        recyclerView.setAdapter(adapter);
                        recyclerView.setLayoutManager(new LinearLayoutManager(Objects.requireNonNull(getView()).getContext(), RecyclerView.VERTICAL, true));

                        layoutBottomSheet.setOnClickListener(view -> {
                            Intent outgoing = new Intent(getActivity(), PatientTabActivity.class);
                            outgoing.putExtra("PATIENT_ID", patient_id);
                            startActivity(outgoing);
                        });

                        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }

                        btnFabMenu.close(true);

                    } else
                        Toast.makeText(getContext(), "Marker tapped", Toast.LENGTH_SHORT).show();
                    return true;
                }

                @Override
                public boolean onItemLongPress(int index, MarkerItem item) {
                    return false;
                }
            });
            mapView.map().layers().add(patientMarkerLayer);
            loadAllPatientsToMap();

            // set theme
            mapView.map().setTheme(VtmThemes.DEFAULT);

            centerMapView();

            // limit the scroll area
            mapView.map().viewport().setMapLimit(bbox);
            mapView.map().viewport().setMinZoomLevel(11);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Move to center of the map view
     */
    private void centerMapView() {
        // set map to center
        mapView.map().animator().animateTo(bbox);
    }

    /**
     * Remove all markers
     */
    private void clearMarkers() {
        if (patientMarkerLayer != null)
            patientMarkerLayer.removeAllItems();
        mapView.map().updateMap(true);
    }

    /**
     * Add Single Patient card in map bottom sheet
     *
     * @param location    - patient location
     * @param patientID   - patient id
     * @param patientName - patient name
     */
    public void addPatientToMap(Location location, String patientID, String patientName) {
        clearMarkers();

        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        MarkerItem patientMarker = MapUtils.createMarker(Objects.requireNonNull(getContext()), location,
                patientName, patientID, R.drawable.ic_location_dark);
        patientMarkerLayer.addItem(patientMarker);
        mapView.map().setMapPosition(location.getLatitude(), location.getLongitude(), 1 << 16);
    }

    /**
     * Database Query change listener
     */
    private QueryChangeListener queryChangeListener = new QueryChangeListener() {
        @Override
        public void changed(QueryChange change) {
            ResultSet resultRows = change.getResults();
            List<Mother> list = new ArrayList<>();
            for (Result row : resultRows)
                list.add(dbMgr.buildPatient(row));

            for (Mother patient : list) {
                if (patient.getLocation() != null && getContext() != null) {
                    Location loc =  GeneralUtils.convertStringToLocation(patient.getLocation());
                    if(loc != null) {
                        MarkerItem patientMarker = MapUtils.createMarker(Objects.requireNonNull(getContext()), loc, patient.getName(), patient.getMainId(), R.drawable.ic_location_dark);
                        // add marker item to layer and update map
                        patientMarkerLayer.addItem(patientMarker);
                    }
                }
            }

            mapView.map().updateMap(true);
            centerMapView();
        }
    };


    /**
     * Load all patients locations to Map
     */
    private void loadAllPatientsToMap() {
        // clear any previous markers in map
        clearMarkers();

        // query for patient records
        try {
            query = dbMgr.getAllPatients();
            queryListenerToken = query.addChangeListener(queryChangeListener);
            query.execute();
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: called");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: called");
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        Log.d(TAG, "onResume: called");

        // If location enabled to restart the location listener
        if (gpsTracker.isLocationEnabled())
            gpsTracker.startLocationListener(locationListener);
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause: called");
        super.onPause();
        mapView.onPause();

        // Stop location listener
        gpsTracker.stopLocationListener();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: called");
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: called");
        super.onDestroy();
        mapView.onDestroy();

        // Remove the location manager
        gpsTracker.destroyLocationManager();

        if (queryListenerToken != null && query != null) {
            query.removeChangeListener(queryListenerToken);
            Log.d(TAG, "onDestroy Listener removed");
        }
    }

    /**
     * Map Events Receiver for Map Gesture Listener
     */
    class MapEventsReceiver extends Layer implements GestureListener {

        MapEventsReceiver(Map map) {
            super(map);
        }

        @Override
        public boolean onGesture(Gesture g, MotionEvent e) {
            if (g instanceof Gesture.Tap) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    if (btnFabMenu.getVisibility() == View.GONE) {
                        layoutBottomSheet.setOnClickListener(null);
                        btnFabMenu.setVisibility(View.VISIBLE);
                    }
                }
                return true;
            }
            if (g instanceof Gesture.LongPress) {
                return false;
            }
            if (g instanceof Gesture.TripleTap) {
                return false;
            }
            return false;
        }
    }
}