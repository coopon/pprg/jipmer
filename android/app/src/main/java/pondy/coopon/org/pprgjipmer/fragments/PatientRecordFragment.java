package pondy.coopon.org.pprgjipmer.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Endpoint;
import com.couchbase.lite.ListenerToken;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryChange;
import com.couchbase.lite.QueryChangeListener;
import com.couchbase.lite.Replicator;
import com.couchbase.lite.ReplicatorConfiguration;
import com.couchbase.lite.Result;
import com.couchbase.lite.ResultSet;
import com.couchbase.lite.URLEndpoint;
import com.github.clans.fab.FloatingActionButton;
import com.google.gson.Gson;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.activity.AddEditBioDataActivity;
import pondy.coopon.org.pprgjipmer.adapters.PatientRecordAdapter;
import pondy.coopon.org.pprgjipmer.helper.DatabaseManager;
import pondy.coopon.org.pprgjipmer.helper.SharedPref;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.rest.ApiClient;
import pondy.coopon.org.pprgjipmer.rest.PPRGInterface;
import pondy.coopon.org.pprgjipmer.utils.AppConfig;
import pondy.coopon.org.pprgjipmer.utils.GeneralUtils;
import pondy.coopon.org.pprgjipmer.utils.ToastUtils;
import pondy.coopon.org.pprgjipmer.utils.constant.PatientRecordConst;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class PatientRecordFragment extends Fragment {

    private static final String TAG = "PatientRecordFragment";
    // Views
    @BindView(R.id.recycler_view_patient)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;
    @BindView(R.id.search_view)
    SearchView searchView;
    // Helper
    private SharedPref sharedPref;
    private DatabaseManager dbMgr;
    // Variables
    private PatientRecordAdapter adapter;
    private Replicator replicator;
    private Query query;
    private ListenerToken queryListenerToken;
    private List<Mother> motherList = new ArrayList<>();
    private Map<Integer, String> localMotherIds = new HashMap<>();
    private List<Integer> serverMotherIds = new ArrayList<>();
    private QueryChangeListener queryChangeListener = new QueryChangeListener() {
        @Override
        public void changed(QueryChange change) {
            ResultSet resultRows = change.getResults();
            motherList.clear();
            localMotherIds.clear();
            for (Result row : resultRows)
            {
                Mother m = dbMgr.buildPatient(row);
                motherList.add(m);
                localMotherIds.put(m.getMotherId(), m.getMainId());
            }
            adapter.setPatients(motherList);
            adapter.notifyDataSetChanged();
        }
    };

    public PatientRecordFragment() {
        Log.d(TAG, "constructor: called");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: called");

        sharedPref = new SharedPref(getContext());
        dbMgr = DatabaseManager.getSharedInstance(getContext());
        adapter = new PatientRecordAdapter(getContext(), PatientRecordConst.BASE);

        try {
            query = dbMgr.getAllPatients();
            queryListenerToken = query.addChangeListener(queryChangeListener);
            query.execute();
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }

        if(motherList.size() > 0)
            syncDataFromServer();
        else
            getDataFromServer();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_patient_record, container, false);

        ButterKnife.bind(this, rootView);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        //When scroll to hide fab
        //GeneralUtils.fabHideOnScroll(recyclerView, fabAdd);

        fabAdd.setVisibility(View.GONE);
        fabAdd.setOnClickListener(view -> {
            Intent outgoing = new Intent(getContext(), AddEditBioDataActivity.class);
            startActivity(outgoing);
        });

        // Refresh listener - call replication
        refreshLayout.setOnRefreshListener(() -> {
            if(motherList.size() > 0)
                syncDataFromServer();
            else
                getDataFromServer();
        });

        /*
         * Search View
         */
        searchView.setIconifiedByDefault(false);
        searchView.setQueryHint(getString(R.string.hint_search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                List<Mother> filterList = new ArrayList<>();
                for (Mother patient : motherList) {
                    String rec = patient.getName() + " " + patient.getDrNo() + " " + patient.getStreetName()
                            + " " + patient.getArea() + " " + patient.getCity() + " " + patient.getPinCode();
                    rec = rec.replace("null", "").toLowerCase();
                    if (rec.contains(newText.toLowerCase())) {
                        filterList.add(patient);
                    }
                }

                adapter.setPatients(filterList);
                adapter.notifyDataSetChanged();

                return false;
            }
        });

        // If it is first time of login to call replicator
        if (sharedPref.isNewLogin()) {
            sharedPref.setNewLogin(false); // Update new login as false
        }



        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void syncDataFromServer() {

        if (GeneralUtils.isNetworkConnected(Objects.requireNonNull(getContext()))) {
            Retrofit retrofit = ApiClient.getRetrofit();
            PPRGInterface pprgInterface = retrofit.create(PPRGInterface.class);


            List<Mother> modifiedMothersList = new ArrayList<>();
            for(Mother mother : motherList){
                Log.e("TAG ", "MODIFIED MOTHER " + mother.getModified());
                if(mother.getModified())
                    modifiedMothersList.add(mother);
            }

            Log.e("TAG"," MODIFIED MOTHETS " + modifiedMothersList);

            pprgInterface.updateMothersVisit(sharedPref.getCenterId(), modifiedMothersList).enqueue(new Callback<List<Mother>>() {
                @Override
                public void onResponse(@NonNull Call<List<Mother>> call, @NonNull Response<List<Mother>> response) {

                    Log.e("TAG", "MODIFIED MOTHER " + new Gson().toJson(response.body()));
                    if (response.isSuccessful() && response.body() != null && response.body().size() > 0) {
                        if(modifiedMothersList.size() > 0) {
                            for (Mother m : modifiedMothersList) {
                                m.setModified(false);
                                try {
                                    dbMgr.createPatient(m);
                                } catch (CouchbaseLiteException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        // Update on local

                        if(response.isSuccessful())
                            serverMotherIds.clear();


                        for (Mother mother : response.body()) {
                            try {
                                ResultSet existingMothers = null;
                                try {
                                    existingMothers = dbMgr.getMotherMainId(mother.getMotherId()).execute();
                                } catch (CouchbaseLiteException e) {
                                    e.printStackTrace();
                                }

                                if(existingMothers != null)
                                {
                                    for(Result res : existingMothers)
                                    {
                                        String mainId = res.getString("id");
                                        Integer motherId = res.getInt("patient_id");
                                        if(mother.getMotherId().equals(motherId)) {
                                            mother.setMainId(mainId);
                                            break;
                                        }
                                    }
                                }
                                serverMotherIds.add(mother.getMotherId());
                                dbMgr.createPatient(mother);
                            } catch (Exception e) {
                                e.printStackTrace();
                                ToastUtils.showError(getActivity(), e.getMessage());
                            }
                        }

                        ToastUtils.showSuccess(getActivity(), getString(R.string.msg_sync_success));


                        //getDataFromServer();
                    }else {
                        Log.e("TAG", "MODIFIED MOTHER ERROR " + new Gson().toJson(response));
                        ToastUtils.showWarning(getContext(), "Something went wrong on sync : " + new Gson().toJson(response.body()));
                        if(refreshLayout != null)
                            refreshLayout.setRefreshing(false);
                    }

                    deleteUnRelatedMothers();

                    if(refreshLayout != null)
                        refreshLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(@NonNull Call<List<Mother>> call, @NonNull Throwable t) {
                    if(refreshLayout != null)
                        refreshLayout.setRefreshing(false);
                    ToastUtils.showWarning(getContext(), t.getMessage());
                    t.printStackTrace();
                }
            });
        } else {
            if(refreshLayout != null)
                refreshLayout.setRefreshing(false);
            ToastUtils.showWarning(getContext(), getString(R.string.msg_check_network_connection));
        }

    }

    private void getDataFromServer(){

        if (GeneralUtils.isNetworkConnected(Objects.requireNonNull(getContext()))) {
            Retrofit retrofit = ApiClient.getRetrofit();
            PPRGInterface pprgInterface = retrofit.create(PPRGInterface.class);
            pprgInterface.getAllMothers(sharedPref.getCenterId()).enqueue(new Callback<List<Mother>>() {
                @Override
                public void onResponse(@NonNull Call<List<Mother>> call, @NonNull Response<List<Mother>> response) {
                    if(response.isSuccessful())
                        serverMotherIds.clear();
                    if (response.isSuccessful() && response.body() != null && response.body().size() > 0) {
                        for (Mother mother : response.body()) {
                            try {
                                Log.e("TAG"," MOTHERS INFO -- ---- " + mother.getMotherId() + " -- " + mother.getName());

                                ResultSet existingMothers = null;
                                try {
                                    existingMothers = dbMgr.getMotherMainId(mother.getMotherId()).execute();
                                } catch (CouchbaseLiteException e) {
                                    e.printStackTrace();
                                }

                                if(existingMothers != null)
                                {
                                    for(Result res : existingMothers)
                                    {
                                        String mainId = res.getString("id");
                                        Integer motherId = res.getInt("patient_id");
                                        if(mother.getMotherId().equals(motherId)) {
                                            mother.setMainId(mainId);
                                            break;
                                        }
                                    }
                                }
                                serverMotherIds.add(mother.getMotherId());
                                dbMgr.createPatient(mother);
                            } catch (Exception e) {
                                e.printStackTrace();
                                ToastUtils.showError(getActivity(), e.getMessage());
                            }
                        }

                        ToastUtils.showSuccess(getActivity(), getString(R.string.msg_sync_success));
                    }else {
                        if(response.body() != null && response.body().size() <= 0)
                            ToastUtils.showNormal(getContext(), "Mothers are yet to be added from serer");
                        else
                            ToastUtils.showWarning(getContext(), "Something went wrong " + new Gson().toJson(response.body()));

                    }

                    deleteUnRelatedMothers();

                    if(refreshLayout != null)
                        refreshLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(@NonNull Call<List<Mother>> call, @NonNull Throwable t) {
                    if(refreshLayout != null)
                        refreshLayout.setRefreshing(false);
                    ToastUtils.showWarning(getContext(), t.getMessage());
                    t.printStackTrace();
                }
            });
        } else {
            if(refreshLayout != null)
                refreshLayout.setRefreshing(false);
            ToastUtils.showWarning(getContext(), getString(R.string.msg_check_network_connection));
        }

    }

    // Delete the local only mothers
    private void deleteUnRelatedMothers() {

        List<String> deletedMotherMainIds = new ArrayList<>();

        for (Integer key : localMotherIds.keySet() ) {
            if(!serverMotherIds.contains(key)) {
                deletedMotherMainIds.add(localMotherIds.get(key));
            }
        }

        Log.e("TAG", "MODIFIED MOTHER DEL  " + localMotherIds.toString() + " del "  +  deletedMotherMainIds.toString());

        if(deletedMotherMainIds.size() > 0)
        {
            for(String mainId : deletedMotherMainIds) {
                try {
                    dbMgr.database.delete(dbMgr.database.getDocument(mainId));
                } catch (CouchbaseLiteException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: called");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called");
        if (searchView != null)
            searchView.clearFocus();
        hideKeyboard();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: called");
        if (queryListenerToken != null && query != null) {
            query.removeChangeListener(queryListenerToken);
            Log.d(TAG, "onDestroy Listener removed");
        }
    }

    private void hideKeyboard() {
        try {
            View focus = getActivity().getCurrentFocus();
            if (focus != null) {
                InputMethodManager imm = (InputMethodManager)  getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}