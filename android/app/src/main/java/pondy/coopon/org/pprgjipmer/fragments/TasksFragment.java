package pondy.coopon.org.pprgjipmer.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.jar.JarEntry;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import info.hoang8f.android.segmented.SegmentedGroup;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.adapters.PatientTaskTypeAdapter;
import pondy.coopon.org.pprgjipmer.helper.DatabaseManager;
import pondy.coopon.org.pprgjipmer.interfaces.PatientTaskUpdateListener;
import pondy.coopon.org.pprgjipmer.interfaces.SubjectUpdateListener;
import pondy.coopon.org.pprgjipmer.model.CheckList;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.model.Subject;
import pondy.coopon.org.pprgjipmer.model.Visit;
import pondy.coopon.org.pprgjipmer.model.VisitQuestion;
import pondy.coopon.org.pprgjipmer.model.VisitType;
import pondy.coopon.org.pprgjipmer.utils.AppConfig;
import pondy.coopon.org.pprgjipmer.utils.GPSTracker;
import pondy.coopon.org.pprgjipmer.utils.GeneralUtils;
import pondy.coopon.org.pprgjipmer.utils.ToastUtils;


public class TasksFragment extends Fragment {

    private static final String TAG = "TasksFragment";


    private DatabaseManager dbMgr;
    private GPSTracker gpsTracker;

    @BindView(R.id.txt_current_visit)
    TextView txtCurrentVisit;
    @BindView(R.id.recycler_view_baby)
    RecyclerView recyclerViewBaby;
    @BindView(R.id.recycler_view_mother)
    RecyclerView recyclerViewMother;
    @BindView(R.id.btn_update)
    Button btnUpdate;
    @BindView(R.id.layout_visit)
    View layoutVisit;
    @BindView(R.id.txt_not_found)
    TextView txtNotFound;
    @BindView(R.id.edit_location)
    EditText editLocation;
    @BindView(R.id.smooth_progress_bar)
    SmoothProgressBar progressBar;
    @BindView(R.id.seg_type)
    SegmentedGroup rgSeqType;

    private Location currentLocation = null;
    private PatientTaskTypeAdapter adapterBaby = null;
    private PatientTaskTypeAdapter adapterMother = null;
    private Mother masterPatient = new Mother();

    public TasksFragment() {
        // Required empty public constructor
    }

    public TasksFragment(Mother masterPatient) {
        this.masterPatient = masterPatient;
        this.dbMgr = DatabaseManager.getSharedInstance(getContext());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tasks, container, false);

        ButterKnife.bind(this, rootView);

        gpsTracker = new GPSTracker(Objects.requireNonNull(getActivity()));

        if (masterPatient.getVisits() != null && masterPatient.getVisits().size() > 0 && masterPatient.getVisits().size() >= masterPatient.getCurrentVisit()) {

            // Get mother current visitOld info
            //VisitOld visitOld = masterPatient.getVisitsInfo().get(masterPatient.getCurrentVisit() - 1);
            int currentVisitIndex = masterPatient.getCurrentVisit() -1;

            CheckList currentCheckList = masterPatient.getVisits().get(currentVisitIndex).getChecklist();



            SubjectUpdateListener babyTaskUpdateListener = (subject) ->{
                masterPatient.getVisits().get(currentVisitIndex).getChecklist().setBaby(subject);
                // Task Update in db
                taskUpdate(false);
            };

            SubjectUpdateListener motherTaskUpdateListener = (subject) ->{
                masterPatient.getVisits().get(currentVisitIndex).getChecklist().setMother(subject);
                // Task Update in db
                taskUpdate(false);
            };



            adapterBaby = new PatientTaskTypeAdapter(getContext(), getVisitTypeList(currentCheckList.getBaby()), babyTaskUpdateListener);
            adapterMother = new PatientTaskTypeAdapter(getContext(), getVisitTypeList(currentCheckList.getMother()), motherTaskUpdateListener);

            recyclerViewBaby.setAdapter(adapterBaby);
            recyclerViewMother.setAdapter(adapterMother);
            recyclerViewBaby.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerViewMother.setLayoutManager(new LinearLayoutManager(getContext()));


            String visitNo = "Visit " + masterPatient.getCurrentVisit();
            txtCurrentVisit.setText(visitNo);

            /*String visitLocation = masterPatient.getVisits().get(currentVisitIndex).getVisitedLocation();

            if(visitLocation != null && visitLocation.contains(",")){
                Location location = GeneralUtils.convertStringToLocation(masterPatient.getVisits().get(currentVisitIndex).getVisitedLocation());
                currentLocation = location;
                String loc = "lat : " + String.format(Locale.ENGLISH, "%.4f", location.getLatitude()) + ", "
                        + "lon : " + String.format(Locale.ENGLISH, "%.4f", location.getLongitude());
                editLocation.setText(loc);
            }else
                editLocation.setText("");*/
            progressBar.setVisibility(View.INVISIBLE);

            /*
             * Location listener
             */
            editLocation.setOnClickListener(view -> {

                // Check location data exist to remove. I.e recall
                if (!TextUtils.isEmpty(editLocation.getText())) {
                    currentLocation = null;
                    editLocation.setText("");
                }

                if (gpsTracker != null) {
                    gpsTracker.initLocationManager(); // Init location manager
                    callLocationListener(); // Call location listener
                }

            });

            /*
             * Right clear drawable click to clear - Location
             */
            editLocation.setOnTouchListener((v, event) -> {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Right drawable
                    if (event.getRawX() >= (editLocation.getRight() - editLocation.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        editLocation.setText("");
                        currentLocation = null;
                        progressBar.setVisibility(View.INVISIBLE);

                        // Destroy location manager
                        gpsTracker.destroyLocationManager();

                        return true;
                    }
                }
                return false;
            });

            btnUpdate.setOnClickListener(view -> {
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.hint_confirmation))
                        .setMessage(getString(R.string.msg_do_you_want_update_visit))
                        .setPositiveButton(android.R.string.yes,
                                (dialog, whichButton) -> {
                                    gpsTracker.stopLocationListener();
                                    // Task Update in db
                                    taskUpdate(true);
                                })
                        .setNegativeButton(android.R.string.no,
                                null)
                        .show();
            });

            recyclerViewBaby.setVisibility(View.VISIBLE);
            recyclerViewMother.setVisibility(View.GONE);
            rgSeqType.setOnCheckedChangeListener((group, checkedId) -> {
                if(checkedId == R.id.radio_mother) {
                    recyclerViewBaby.setVisibility(View.GONE);
                    recyclerViewMother.setVisibility(View.VISIBLE);
                    recyclerViewMother.smoothScrollToPosition(0);
                }else {
                    recyclerViewBaby.setVisibility(View.VISIBLE);
                    recyclerViewMother.setVisibility(View.GONE);
                    recyclerViewBaby.smoothScrollToPosition(0);
                }
            });

        } else {
            txtNotFound.setVisibility(View.VISIBLE);
            layoutVisit.setVisibility(View.GONE);
            txtCurrentVisit.setVisibility(View.GONE);

            boolean isDoneAll = masterPatient.getVisits() != null && masterPatient.getVisits().size() < masterPatient.getCurrentVisit();
            txtNotFound.setText(isDoneAll ? R.string.msg_done_visit_planned : R.string.msg_no_visit_planned);
            txtNotFound.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getContext()), isDoneAll ? R.color.toast_success : R.color.toast_error));
        }

        return rootView;
    }

    private List<VisitType> getVisitTypeList(Subject subject){
        List<VisitType> visitTypeList = new ArrayList<>();
        if(subject.getAsk() !=null)
            visitTypeList.add(new VisitType("Ask", subject.getAsk(), AppConfig.QuestionTypeAsk));

        if(subject.getExamine() != null)
            visitTypeList.add(new VisitType("Examine", subject.getExamine(), AppConfig.QuestionTypeExamine));


        if(subject.getCounsel() != null)
            visitTypeList.add(new VisitType("Counsel", subject.getCounsel(), AppConfig.QuestionTypeCounsel));

        return visitTypeList;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("TAG", "LOCATION  GETTTT 1");
        if(requestCode == GPSTracker.CODE_LOCATION_PERMISSION) {
            Log.e("TAG", "LOCATION  GETTTT 2");
            if(resultCode == Activity.RESULT_CANCELED){
                Log.e("TAG", "LOCATION  GETTTT 3");
                gpsTracker.showSettingsAlert();
            }else if(resultCode == Activity.RESULT_OK && gpsTracker != null && gpsTracker.isLocationEnabled()){
                callLocationListener();
                Log.e("TAG", "LOCATION  GETTTT 4");
            }
            Log.e("TAG", "LOCATION  GETTTT 5");
        }
    }

    /**
     * Task Update in db
     *
     * @param isVisitCompleted - true to visit update otherwise update only tasks
     */
    private void taskUpdate(boolean isVisitCompleted) {
        // Init location manager
        //gpsTracker.initLocationManager();

        if (true || currentLocation != null) {
            try {
                Log.e("TAG " , "LOCATION " + currentLocation);
                // Update values
                if(currentLocation != null)
                    masterPatient.getVisits().get(masterPatient.getCurrentVisit() -1).setVisitedLocation(currentLocation.getLatitude() +"," +currentLocation.getLongitude());
                dbMgr.updateVisitStatus(masterPatient.getMainId(), masterPatient.getVisits().get(masterPatient.getCurrentVisit() -1), isVisitCompleted);
                if (isVisitCompleted) {
                    ToastUtils.showSuccess(getContext(), getString(R.string.msg_visit_updated));
                    Objects.requireNonNull(getActivity()).finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
                ToastUtils.showError(getContext(), getString(R.string.error_couchbase_exception));
            }
        } else {
            // If location enabled to fetch location take some time. So, showing the progress
            /*if (gpsTracker.isLocationEnabled()) {
                progressBar.setVisibility(View.VISIBLE);
                ToastUtils.showShort(getContext(), "Getting visit location...");
            }*/
            // Start location listener
            //gpsTracker.startLocationListener(locationListener);
        }

    }

    // Location listener
    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            // Update in location layer & map view
            currentLocation = location;

            if (currentLocation != null) {
                progressBar.setVisibility(View.INVISIBLE);
                masterPatient.setLocation(currentLocation.getLatitude() + "," + currentLocation.getLongitude());
                String loc = "lat : " + String.format(Locale.ENGLISH, "%.4f", currentLocation.getLatitude()) + ", "
                        + "lon : " + String.format(Locale.ENGLISH, "%.4f", currentLocation.getLongitude());
                editLocation.setText(loc);
                gpsTracker.stopLocationListener();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {
            progressBar.setVisibility(View.INVISIBLE);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called");

        // If location enabled to restart the location listener
        if (gpsTracker.isLocationEnabled() && currentLocation == null)
            callLocationListener();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: called");
        // Stop location listener
        gpsTracker.stopLocationListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: called");
        // Remove the location manager
        gpsTracker.destroyLocationManager();
    }

    private void callLocationListener() {

        if (gpsTracker != null) {
            // Show loading progress
            if (gpsTracker.checkLocationPermission() && gpsTracker.isLocationEnabled())
                progressBar.setVisibility(View.VISIBLE);
            // Start the location listener
            gpsTracker.startLocationListener(locationListener);
        }
    }

}
