package pondy.coopon.org.pprgjipmer.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.adapters.TimeLineAdapter;
import pondy.coopon.org.pprgjipmer.model.CheckList;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.model.Subject;
import pondy.coopon.org.pprgjipmer.model.Task;
import pondy.coopon.org.pprgjipmer.model.TimeLineModel;
import pondy.coopon.org.pprgjipmer.model.Visit;
import pondy.coopon.org.pprgjipmer.model.VisitType;
import pondy.coopon.org.pprgjipmer.utils.AppConfig;
import pondy.coopon.org.pprgjipmer.utils.constant.VisitStatus;


public class VisitHistoryFragment extends Fragment {


    @BindView(R.id.txt_no_visits_planned)
    TextView txtNoVisitsPlanned;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.layout_visit_title)
    RelativeLayout layoutVisitTitle;
    private Mother masterPatient = new Mother();

    public VisitHistoryFragment() {
        // Required empty public constructor
    }

    public VisitHistoryFragment(Mother masterPatient) {
        this.masterPatient = masterPatient;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_visit_history, container, false);

        ButterKnife.bind(this, rootView);

        List<TimeLineModel> mDataList = new ArrayList<>();
        if (masterPatient.getVisits() != null && masterPatient.getVisits().size() >0) {


            int status = VisitStatus.COMPLETED;
            int i = 1;
           for (Visit visit : masterPatient.getVisits()) {

                if (masterPatient.getCurrentVisit() > i)
                    status = VisitStatus.COMPLETED;
                else if (masterPatient.getCurrentVisit() == i)
                    status = VisitStatus.ACTIVE;
                else if (masterPatient.getCurrentVisit() < i)
                    status = VisitStatus.INACTIVE;

                mDataList.add(new TimeLineModel((i++), visit.getSchedDate(), visit.getVisitedDate(), getTaskList(visit.getChecklist()), status));

            }

            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);

            TimeLineAdapter mAdapter = new TimeLineAdapter(getContext(), mDataList);
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            txtNoVisitsPlanned.setVisibility(View.GONE);
        }else
        {
            layoutVisitTitle.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            txtNoVisitsPlanned.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    private List<Task> getTaskList(CheckList checklist) {
        List<Task> taskList = new ArrayList<>();

        if(checklist != null && checklist.getBaby() != null) {
            taskList.addAll(getModifiedTaskList("B", checklist.getBaby().getAsk()));
            taskList.addAll(getModifiedTaskList("B", checklist.getBaby().getExamine()));
            taskList.addAll(getModifiedTaskList("B", checklist.getBaby().getCounsel()));
        }

        if(checklist != null && checklist.getMother() != null) {
            taskList.addAll(getModifiedTaskList("M", checklist.getMother().getAsk()));
            taskList.addAll(getModifiedTaskList("M", checklist.getMother().getExamine()));
            taskList.addAll(getModifiedTaskList("M", checklist.getMother().getCounsel()));
        }

        return taskList;
    }

    private List<Task> getModifiedTaskList(String prefix, List<Task> taskList){
        List<Task> list = new ArrayList<>();
        for(Task task : taskList)
            list.add(new Task(prefix + " - " + task.getQuestion(), task.getAnswer()));

        return list;
    }
}
