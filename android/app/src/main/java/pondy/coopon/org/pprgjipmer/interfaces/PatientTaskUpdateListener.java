package pondy.coopon.org.pprgjipmer.interfaces;

public interface PatientTaskUpdateListener {
    public void onClick(int position, boolean isChecked, String tag);
}
