package pondy.coopon.org.pprgjipmer.interfaces;

import java.util.List;

import pondy.coopon.org.pprgjipmer.model.Task;

public interface TaskUpdateListener {
    void onTaskUpdate(String tag, List<Task> taskList);
}
