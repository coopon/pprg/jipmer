package pondy.coopon.org.pprgjipmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckList {

    @SerializedName("baby")
    @Expose
    private Subject baby;
    @SerializedName("mother")
    @Expose
    private Subject mother;

    public Subject getBaby() {
        return baby;
    }

    public void setBaby(Subject baby) {
        this.baby = baby;
    }

    public Subject getMother() {
        return mother;
    }

    public void setMother(Subject mother) {
        this.mother = mother;
    }

    @Override
    public String toString() {
        return "CheckList{" +
                "baby=" + baby +
                ", mother=" + mother +
                '}';
    }
}