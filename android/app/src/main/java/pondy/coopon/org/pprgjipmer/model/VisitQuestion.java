package pondy.coopon.org.pprgjipmer.model;

import java.io.Serializable;

public class VisitQuestion implements Serializable {

    private String question;
    private Boolean answer;

    public VisitQuestion() {
    }

    public VisitQuestion(String question, Boolean answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Boolean getAnswer() {
        return answer;
    }

    public void setAnswer(Boolean answer) {
        this.answer = answer;
    }
}
