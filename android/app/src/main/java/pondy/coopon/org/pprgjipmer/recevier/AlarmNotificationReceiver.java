package pondy.coopon.org.pprgjipmer.recevier;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Query;
import com.couchbase.lite.Result;
import com.couchbase.lite.ResultSet;

import java.util.ArrayList;
import java.util.List;

import androidx.core.app.NotificationCompat;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.activity.MainActivity;
import pondy.coopon.org.pprgjipmer.helper.DatabaseManager;
import pondy.coopon.org.pprgjipmer.helper.SharedPref;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.utils.DateTimeUtils;

public class AlarmNotificationReceiver extends BroadcastReceiver {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("ALARM - CALL ", "AlarmNotificationReceiver init");

        SharedPref pref = new SharedPref(context);
        if(pref.isLoggedIn())
            showNotification(context);
    }

    private void showNotification(Context context) {

        DatabaseManager dbMgr = DatabaseManager.getSharedInstance(context);

        List<Mother> list = new ArrayList<>();

        // Get today visits information
        try {
            Query query = dbMgr.getScheduleRecordsOnParticularDate(DateTimeUtils.dateNow(DateTimeUtils.dateFormat_yyyy_MM_dd));
            ResultSet resultSet = query.execute();
            for (Result result : resultSet)
                list.add(dbMgr.buildPatient(result));
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }


        if (list.size() > 0) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            String channelId = context.getString(R.string.notify_channel_id);
            String notifyTitle = context.getString(R.string.notify_title);

            /* Add Big View Specific Configuration */
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

            // Sets a title for the Inbox style big view
            inboxStyle.setBigContentTitle(context.getString(R.string.notify_big_content_title));

            // Moves events into the big view
            int i = 1;
            for (Mother patient : list) {
                inboxStyle.addLine(i++ + ". " + patient.getName() + ", " + patient.getArea());
            }

            String contentText = "You have " + list.size() + (list.size() > 1 ? " visits" : " visit") + " today.";

            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                CharSequence channelName = context.getString(R.string.notify_channel_name);
                String description = context.getString(R.string.notify_channel_description);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
                notificationChannel.setDescription(description);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(true);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300});
                notificationManager.createNotificationChannel(notificationChannel);

                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                notificationManager.createNotificationChannel(notificationChannel);
            }

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.ic_mascot)
                    .setContentTitle(notifyTitle)
                    .setContentText(contentText)
                    .setStyle(inboxStyle)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            Intent notificationIntent = new Intent(context, MainActivity.class);
            notificationIntent.setAction("OPEN_SCHEDULE_FRAGMENT");
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, Intent.FILL_IN_ACTION);
            builder.setContentIntent(contentIntent);

            // notificationId is a unique int for each notification that you must define
            notificationManager.notify(201, builder.build());
        } else
            Log.e("ALARM - CALL", "No notification have");

    }
}