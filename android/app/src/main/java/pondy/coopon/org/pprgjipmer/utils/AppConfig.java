package pondy.coopon.org.pprgjipmer.utils;

public class AppConfig {

    public static final String DBNAME = "/jipmer";
    private static final String HOST = "192.168.1.12:4984";

    public static final String WS_API_URL = "ws://" + HOST;

    public static final String API_BASE_URL = "https://pprg.cooponscitech.in/";
    public static final String API_AUTHENTICATE = "users/authenticate";
    public static final String API_GET_MOTHERS = "/center/{center_id}/mothers/all";
    public static final String API_POST_MOTHERS_VISITS = "/center/{center_id}/mothers/sync";

    public static final String QuestionTypeAsk = "ASK";
    public static final String QuestionTypeExamine = "EXAMINE";
    public static final String QuestionTypeCounsel = "COUNSEL";

}