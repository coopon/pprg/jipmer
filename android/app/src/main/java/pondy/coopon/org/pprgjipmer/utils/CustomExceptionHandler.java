package pondy.coopon.org.pprgjipmer.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class CustomExceptionHandler implements Thread.UncaughtExceptionHandler {

    private Thread.UncaughtExceptionHandler defaultUEH;

    private String localPath;
    private String[] mailIds;
    private Context context;

    /* 
     * if any of the parameters is null, the respective functionality 
     * will not be used 
     */
    public CustomExceptionHandler(Context  context, String localPath, String[] mailIds) {
        this.localPath = localPath;
        this.mailIds = mailIds;
        this.context = context;
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
    }

    public void uncaughtException(Thread t, @NonNull Throwable e) {
        String timestamp = DateTimeUtils.dateNow(DateTimeUtils.dateFormat_YYYY_MM_dd_hh_mm_ss);
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        String stacktrace = result.toString();
        printWriter.close();
        String filename = timestamp + ".stacktrace";

        if (localPath != null) {
            writeToFile(stacktrace, filename);
        }
        if (mailIds != null && mailIds.length > 0) {
            sendToServer(stacktrace, filename, mailIds);
        }

        defaultUEH.uncaughtException(t, e);
    }

    private void writeToFile(String stacktrace, String filename) {
        try {
            BufferedWriter bos = new BufferedWriter(new FileWriter(
                    localPath + "/" + filename));
            bos.write(stacktrace);
            bos.flush();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendToServer(String stacktrace, String filename, String[] mailIds) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, mailIds);
        intent.putExtra(Intent.EXTRA_SUBJECT, "PPRJ App - Crash Report ");
        String start = "----------------------- START -------------------------";
        String end = "----------------------- END -------------------------";
        intent.putExtra(Intent.EXTRA_TEXT, start +"\n" + filename + "\n"  + "\n" + end);
        Toast.makeText(context, "Hi", Toast.LENGTH_LONG).show();
        context.startActivity(Intent.createChooser(intent, "Send via email ..."));
    }
}