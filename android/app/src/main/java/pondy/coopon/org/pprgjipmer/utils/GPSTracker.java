package pondy.coopon.org.pprgjipmer.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import pondy.coopon.org.pprgjipmer.BuildConfig;
import pondy.coopon.org.pprgjipmer.R;

public class GPSTracker {

    public static final int CODE_LOCATION_PERMISSION = 100;
    public static final int CODE_GPS_ENABLE = 1;

    // The minimum distance to change Updates in meters
    private long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 0 meters

    // The minimum time between updates in milliseconds
    private long MIN_TIME_BW_UPDATES = 1000; // 1sec


    private LocationManager locationManager = null;
    private LocationListener locationListener;

    private static final int ONE_MINUTE = 1000 * 60;

    private Location location;

    private Activity mActivity;
    private Context mContext;

    /**
     * GPS Tracker - constructor
     */
    public GPSTracker(Activity activity) {
        this.mActivity = activity;
        this.mContext = activity.getApplicationContext();
    }

    /**
     * Init Location manager
     */
    public void initLocationManager() {
        if (locationManager == null)
            this.locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
    }

    /**
     * Destroy or remove location manager
     */
    public void destroyLocationManager() {
        stopLocationListener();
        this.locationManager = null;
    }

    /**
     * Set Minimum distance changes for updates
     */
    public void setMinDistanceChangeForUpdates(long minDistanceChangeForUpdates) {
        MIN_DISTANCE_CHANGE_FOR_UPDATES = minDistanceChangeForUpdates;
    }

    /**
     * Set Minimum time for updates
     */
    public void setMinTimeBwUpdates(long minTimeBwUpdates) {
        MIN_TIME_BW_UPDATES = minTimeBwUpdates;
    }

    /**
     * Check location permissions are granted or not
     */
    public boolean checkLocationPermission() {
        return (
                ActivityCompat.checkSelfPermission(mContext,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(mContext,
                                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
        );
    }

    private boolean locationPermissionDenied(){
        return ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED && ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED;
    }

    private void openAppPermissionSettings(){
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        mActivity.startActivity(intent);
    }

    /**
     * Request to grand location permission
     */
    private void requestLocationPermission() {

        if(locationPermissionDenied()){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);

            // Setting Dialog Title
            alertDialog.setTitle(mContext.getString(R.string.location_permission_ask_dialog_title));

            // Setting Dialog Message
            alertDialog.setMessage(mContext.getString(R.string.location_permission_ask_dialog_msg));

            // On pressing Settings button
            alertDialog.setPositiveButton(mContext.getString(R.string.btn_settings), (dialog, which) -> {
                openAppPermissionSettings();
            });

            // on pressing cancel button
            alertDialog.setNegativeButton(mContext.getString(R.string.btn_cancel), (dialog, which) -> dialog.cancel());

            // Showing Alert Message
            alertDialog.show();

        }else {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}, CODE_LOCATION_PERMISSION);
        }
    }

    /**
     * Check Location enable or not
     */
    public boolean isLocationEnabled() {
        return locationManager != null && !locationPermissionDenied() && (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
    }

    /**
     * Check current location is best location
     */
    public boolean isBetterLocation(Location location, Location currentBestLocation) {

        if (currentBestLocation == null)
            // A new location is always better than no location
            return true;

        // check whether new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > ONE_MINUTE;
        boolean isSignificantlyOlder = timeDelta < ONE_MINUTE;

        if (isSignificantlyNewer) {
            return true;
        } else if (isSignificantlyOlder) {
            return false;
        }

        // check whether if new location is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // check if both locations are from same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        if (isMoreAccurate)
            return true;
        else if (!isLessAccurate)
            return true;
        else return !isSignificantlyLessAccurate && isFromSameProvider;

    }

    /**
     * Check provider is same or diffrest
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null)
            return provider2 == null;

        return provider1.equals(provider2);
    }

    /**
     * Start location listener
     *
     * @param locListener - listener object
     */
    public void startLocationListener(LocationListener locListener) {
        // Init listener
        locationListener = locListener;

        // Check permission status
        if (checkLocationPermission()) {
            // Check Location status
            if (isLocationEnabled()) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
            } else
                showSettingsAlert();
        } else
            requestLocationPermission();

    }

    /**
     * Stop using GPS listener
     */
    public void stopLocationListener() {
        if (locationManager != null && locationListener != null) {
            locationManager.removeUpdates(locationListener);
        }
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);

        // Setting Dialog Title
        alertDialog.setTitle(mContext.getString(R.string.location_ask_dialog_title));

        // Setting Dialog Message
        alertDialog.setMessage(mContext.getString(R.string.location_ask_dialog_msg));

        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.btn_settings), (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            mActivity.startActivityForResult(intent, CODE_GPS_ENABLE);
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(mContext.getString(R.string.btn_cancel), (dialog, which) -> dialog.cancel());

        // Showing Alert Message
        alertDialog.show();
    }

}