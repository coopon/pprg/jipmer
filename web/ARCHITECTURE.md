# pprg.web ARCHITECTURE

# Data Models

## Worker

| Column  | Data Type    |
| ---     | ---          |
| id      | SERIAL       |
| name    | VARCHAR(255) |
| mobile  | VARCHAR(10)  |
| officer | BOOLEAN      |
|         |              |

## Mother

| Column    | Data Type    |
| ---       | ---          |
| id        | VARCHAR(15)  |
| name      | VARCHAR(200) |
| pregnancy | DATE         |
| address   | VARCHAR(255) |
| latlon    | VARCHAR(50)  |
|           |              |

## Meeting 
| Column   | Data Type  |
| ---      | ---        |
| user     | FOREIGNKEY |
| mother   | FOREIGNKEY |
| date     | DATE       |
| gpx      | TEXT       |
| services | JSON?      |
|          |            |

# REST API

## /login/ - POST 

## /register/ - POST 

## /mother/:id - GET

## /mother/:id - UPDATE

## /


## Authentication 

## 
