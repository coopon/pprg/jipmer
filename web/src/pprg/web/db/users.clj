(ns pprg.web.db.users
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "pprg/web/db/sql/users.sql")
