(ns pprg.web.service
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [io.pedestal.http.ring-middlewares :as middlewares]
            [io.pedestal.interceptor :as interceptor]
            [io.pedestal.interceptor.chain :as interceptor.chain]
            [io.pedestal.interceptor.error :refer [error-dispatch]]
            [ring.util.response :as ring-resp]
            [ring.middleware.session.cookie :as cookie]
            [buddy.auth :as auth]
            [buddy.auth.backends :as auth.backends]
            [buddy.auth.middleware :as auth.middleware]
            [pprg.web.site.auth :as site.auth]
            [pprg.web.site.home :as site.home]
            [pprg.web.site.user :as site.user]
            [pprg.web.site.mother :as site.mother]
            ;; [mount.core :refer defstate]
            ))

(def secret (:secret (clojure.edn/read-string (slurp "resources/env/secret.edn"))))

(defn unauthorized-handler [request metadata]
;;(if
  (ring-resp/redirect "/login"))

(def backend (auth.backends/session {:unauthorized-handler unauthorized-handler}))

(defn admin
  "Returns a 200 response for authorized users, otherwise throws a buddy-auth 'unauthorized' exception."
  [request]
  (let [known-user  (:identity request)]
    (if (= :admin (:role known-user))
      (ring-resp/response  "Only admins can see this!")
      (auth/throw-unauthorized))))

(defn about-page
  [request]
  (->  (ring-resp/redirect (format "/"))
       (assoc :status 302)))


;; https://github.com/pedestal/pedestal/blob/master/samples/buddy-auth/src/buddy_auth/service.clj


(def authentication-interceptor
  "Port of buddy-auth's wrap-authentication middleware."
  (interceptor/interceptor
   {:name ::authenticate
    :enter (fn [ctx]
             (update
              ctx
              :request auth.middleware/authentication-request backend))}))

(defn authorization-interceptor
  "Port of buddy-auth's wrap-authorization middleware."
  [backend]
  (error-dispatch [ctx ex]
                  [{:exception-type :clojure.lang.ExceptionInfo :stage :enter}]
                  (try
                    (assoc ctx
                           :response
                           (auth.middleware/authorization-error (:request ctx)
                                                                ex
                                                                backend))
                    (catch Exception e
                      (assoc ctx ::interceptor.chain/error e)))

                  :else (assoc ctx ::interceptor.chain/error ex)))


;; Defines "/" and "/about" routes with their associated :get handlers.
;; The interceptors defined after the verb map (e.g., {:get home-page}
;; apply to / and its children (/about).


;; (def common-interceptors [(body-params/body-params) http/html-body])


(def common-interceptors [(body-params/body-params) http/html-body authentication-interceptor (authorization-interceptor backend)])

;; Tabular routes


(def routes #{["/" :get (conj common-interceptors `site.home/home-page)]
              ["/about" :get (conj common-interceptors `about-page)]
              ["/admin" :get (conj common-interceptors `admin)]
              ["/user/new" :get (conj common-interceptors `site.user/create-worker-page)]
              ["/user/post" :post (conj common-interceptors `site.user/create-user-post)]
              ["/mother/new" :get (conj common-interceptors `site.mother/create-mother-page)]
              ["/mother/post" :post (conj common-interceptors `site.mother/create-mother-post)]
              ;;            ["/login" :get (conj common-interceptors `site.auth/auth-page)]
              })

;; Map-based routes
;(def routes `{"/" {:interceptors [(body-params/body-params) http/html-body]
;                   :get home-page
;                   "/about" {:get about-page}}})

;; Terse/Vector-based routes
;(def routes
;  `[[["/" {:get home-page}
;      ^:interceptors [(body-params/body-params) http/html-body]
;      ["/about" {:get about-page}]]]])


;; Consumed by pprg.web.server/create-server
;; See http/default-interceptors for additional options you can configure


(def service {:env :prod
              ;; You can bring your own non-default interceptors. Make
              ;; sure you include routing and set it up right for
              ;; dev-mode. If you do, many other keys for configuring
              ;; default interceptors will be ignored.
              ;; ::http/interceptors []
              ::http/routes routes

              ;; Uncomment next line to enable CORS support, add
              ;; string(s) specifying scheme, host and port for
              ;; allowed source(s):
              ;;
              ;; "http://localhost:8080"
              ;;
              ;;::http/allowed-origins ["scheme://host:port"]

              ;; Tune the Secure Headers
              ;; and specifically the Content Security Policy appropriate to your service/application
              ;; For more information, see: https://content-security-policy.com/
              ;;   See also: https://github.com/pedestal/pedestal/issues/499
              ;;::http/secure-headers {:content-security-policy-settings {:object-src "'none'"
              ;;                                                          :script-src "'unsafe-inline' 'unsafe-eval' 'strict-dynamic' https: http:"
              ;;                                                          :frame-ancestors "'none'"}}

              ;; Root for resource interceptor that is available by default.
              ::http/resource-path "/public"

              ;; Either :jetty, :immutant or :tomcat (see comments in project.clj)
              ;;  This can also be your own chain provider/server-fn -- http://pedestal.io/reference/architecture-overview#_chain_provider
              ::http/enable-session {:store (cookie/cookie-store {:key secret})}
              ::http/type :jetty
              ;;::http/host "localhost"
              ::http/port 7000
              ;; Options to pass to the container (Jetty)
              ::http/container-options {:h2c? true
                                        :h2? false
                                        ;:keystore "test/hp/keystore.jks"
                                        ;:key-password "password"
                                        ;:ssl-port 8443
                                        :ssl? false}})

