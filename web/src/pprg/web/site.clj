(ns pprg.web.site
  (:require  [ring.util.response :as ring-resp])
  (:use
   [hiccup.page :only (html5 include-css include-js)]))

(defn base [title & content]
  (html5 {:lang "en"}
         [:head
          [:meta {:charset "utf-8"}]
          [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
          [:link {:rel "icon" :type "image/png" :href "/img/favicon.png"}]
          [:title title]
          (include-css "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css")]
         content))

(defn form-control
  "Default form control for dynamic deployment"
  [text]
  [:div.field [:label.label text] [:div.control [:input.input {:type "text" :placeholder text :name (clojure.string/lower-case text)}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-email
  "Default form control for dynamic deployment"
  [text]
  [:div.field [:label.label text] [:div.control [:input.input {:type "email" :placeholder text :name text}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-password
  "Password form control for dynamic deployment"
  [text]
  [:div.field [:label.label text] [:div.control [:input.input {:type "password" :placeholder text :name (clojure.string/lower-case text)}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-dropdown
  "Dropdown form control for dynamic deployment"
  [text arguments]
  [:div.field
   [:label.label text]
   [:span.select
    [:select
     (map #(vector :option (str %)) arguments)]
    [:span.icon.is-small.is-left [:i.fas.fa-globe]]]])

(defn form-control-button [name]
  [:div.control [:button.button.is-primary name]])
