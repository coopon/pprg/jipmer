(ns pprg.web.site.auth
  (:require [buddy.hashers :as hashers]
            [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [pprg.web.site :as site]
            [pprg.web.db.users :as users]
            [pprg.web.db :as db]))

;;(defn user-by-email-exists? [email]
;;  "Checks if user for the given email exists"
;;  (let [data (users/users-by-email db/jdbc email)]
;;    (nil? data)))
;; https://juxt.pro/blog/posts/securing-your-clojurescript-app.html


;; (defn authenticate [data]
;;   "Authenticates user credentials from db"
;;   (let [datum (users/users-by-email (:email data))]))

;; (defn new-user [data]
;;   "Creates a new user in the db"
;;   (println data)
;;   (let [datum
;;         {:email (:email data)
;;          :password (hashers/derive (:password data) {:alg :bcrypt+blake2b-512})}]
;;     (println datum)
;;     (if (user-by-email-exists? datum)
;;       (users/users-create db/jdbc datum)
;;       (println "User exists"))))

(defn login-post [request]
  (-> (ring-resp/redirect "/")
      (assoc :session {:token "token"})))

(defn register-post [request]
  (-> (ring-resp/redirect "/")
      (assoc :session {:token "token"})))

(defn auth-page [request]
  (ring-resp/response (site/base
                       "Login or Register"
                       [:h1 "hello"])))

(defn auth-control []
  (hiccup/html
   [:div.field
    [:label.label "Email"]
    [:div.control
     [:input.input {:type "email" :placeholder "yourname@mail.com"}]]]
   [:div.field
    [:label.label "Password"]
    [:div.control
     [:input.input {:type "password" :placeholder "yourname@mail.com"}]]]))
