(ns pprg.web.site.user
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [pprg.web.site :as site]
            [pprg.web.site.auth :as site.auth]
            [buddy.auth :as buddy.auth]
            [clj-http.client :as client]))

(defn create-worker-page [request]
  (ring-resp/response (site/base "Create Health Worker"
                                 [:div.column.is-half.is-offset-one-quarter
                                  [:h1.title.has-text-centered "Create Health Worker"]
;;                                 [:h2.has-size-3 "Hello"]
;;                                 [:h2.has-size-3 "Kek"]

                                  [:form {:method "POST" :action "/user/post"}
                                   (site/form-control "Name")
                                   (site/form-control "Username")
                                   (site/form-control-password "Password")
                                   (site/form-control-email  "Email")
                                   (site/form-control-dropdown "Center" ["Kurichikuppam" "Lawspet"])
                                   (site/form-control-button "Submit")]])))

(defn worker-profile-control [request]
  [])

(defn create-user-couch
  "Creates a Sync gateway user"
  [request]
  (let [params (:params request)]
    (client/post "http://localhost:4985/jipmer/"
                 {:form-params
                  {:name (:name params)  ,
                   :password (:password params),
                   :admin_channels  [(:username params)
                                     "Kurichikuppam"],
                   :admin_roles ["nurse"],
                   :email (:email params),
                   :disabled true}

                  :content-type :json})))


  ;; {
 ;;  "name": "string",
;;   "password": "string",
;;   "admin_channels": [
  ;;     {{username}}
  ;;      {{health centre}}
;;   ],
;;   "admin_roles": [
;;     {{worker}}
;;   ],
;;   "email": "string",
;;   "disabled": true
  ;; }


(defn create-user-seed-data
  "Seed initial data for the syncgateway user"
  [request])

(defn create-user-post
  "Accepts new user form POST data and processes the request"
  [request]
  (ring-resp/response (str (:params request))))
